#!/bin/sh
# This script is released under the GPLv3 license
# See COPYING file for more information

isInstalled(){
	command -v "$@" >/dev/null 2>&1 || { echo "Programm $@ is not installed but needed. Please install this programm first!"; exit 1; }
}

createFiles(){
        if [ ! -f "$PRG_PATH""/time_MOFR.txt" ]
                then
                        echo "-1" > "$PRG_PATH""/time_MOFR.txt"
        fi
        if [ ! -f "$PRG_PATH""/time_SASU.txt" ]
                then
                        echo "-1" > "$PRG_PATH""/time_SASU.txt"
        fi
        if [ ! -f "$PRG_PATH""/week_MOFR.txt" ]
                then
                        echo "-1" > "$PRG_PATH""/week_MOFR.txt"
        fi
        if [ ! -f "$PRG_PATH""/week_SASU.txt" ]
                then
                        echo "-1" > "$PRG_PATH""/week_SASU.txt"
        fi
}

copyFiles() {
	cp timecheck.conf.example "$PRG_PATH/timecheck.conf" || { echo "An error occured during copying config file!"; exit 1; }
	cp timecheck.sh "$PRG_PATH/timecheck.sh" || { echo "An error occured during copying main script!"; exit 1; }
}

getUser(){
	USER=$(whiptail --inputbox "Which user should be controlled by Timecheck?" 8 60 --title "Timecheck konfiguration" 3>&1 1>&2 2>&3)
	EXITCODE="$?"
	if [ "$EXITCODE" = "0" ]
		then
			if [ -z "$USER" ]
				then
					echo "Invalid username! Script execution aborted."
					exit 1
			fi
		else
			echo "An error occured! Script execution aborted."
			exit 1
	fi
}

getTime(){
	TIME_MOFR=$(whiptail --inputbox "How many minutes should $USER be allowed to use the account from monday to friday?" 8 60 --title "Timecheck configuration" 3>&1 1>&2 2>&3)
	EXITCODE="$?"
	if [ ! "$EXITCODE" = "0" ]
		then
			echo "Invalid input! Script execution aborted."
			exit 1
	fi
	TIME_SASU=$(whiptail --inputbox "How many minutes should $USER be allowed to use the account at the weekend?" 8 60 --title "Timecheck configuration" 3>&1 1>&2 2>&3)
	EXITCODE="$?"
	if [ ! "$EXITCODE" = "0" ]
		then
			echo "Invalid input! Script execution aborted."
			exit 1
	fi
}

checkPermissions(){
	INSTALL_USER=$(whoami)
	if [ ! "$INSTALL_USER" = "root" ]
		then
			echo "You don't have enough permissions to perform this script. Please retry as root. Script execution aborted."
			exit 1
	fi
}


getPath(){
	PRG_PATH=$(whiptail --inputbox "Where should timecheck be installed?" 8 60 /opt/timecheck/ --title "Timecheck configuration" 3>&1 1>&2 2>&3)
        EXITCODE="$?"
        if [ ! "$EXITCODE" = "0" ]
                then
                        echo "Invalid input! Script execution aborted."
                        exit 1
        fi
        case $PRG_PATH in
                */)
                ;;
                *) PRG_PATH=$PRG_PATH/
                ;;
        esac
}

createDir(){
	if [ ! -d "$PRG_PATH" ]
		then
			mkdir "$PRG_PATH"
	fi
}

setFilePermission(){
	chown root:root -R "$PRG_PATH"
        EXITCODE="$?"
        if [ ! "$EXITCODE" = "0" ]
                then
                        echo "An error occured! Script execution aborted."
                        exit 1
        fi

	chmod 700 -R "$PRG_PATH"
        EXITCODE="$?"
        if [ ! "$EXITCODE" = "0" ]
                then
                        echo "An error occured! Script execution aborted."
                        exit 1
        fi
	chown root:root timecheck.sh
	chmod 744 timecheck.sh
}

exportVariable(){
	sed -i 's/USER=YourUserHere/USER='"$USER"'/' "$PRG_PATH""timecheck.conf"
	sed -i 's/TIME_MOFR=100/TIME_MOFR='"$TIME_MOFR"'/' "$PRG_PATH""timecheck.conf"
	sed -i 's/TIME_SASU=100/TIME_SASU='"$TIME_SASU"'/' "$PRG_PATH""timecheck.conf"
	echo "$PRG_PATH" > timecheck_path.txt
}

exportCronjob(){
	echo ""
	echo "We are allmost done. Just one thing is missing"
	echo "Please add the following to the end of the root crontab"
	echo "open the crontab with 'sudo crontab -e'"
	echo ""
	echo "@reboot \"$PRG_PATH""timecheck.sh\""
	echo "* * * * * \"$PRG_PATH""timecheck.sh\""
	echo ""

}
main(){
	checkPermissions
	for PRG in sudo notify-send
	do
		isInstalled $PRG
	done
	getPath
	getUser
	getTime
	createDir
	copyFiles
	createFiles
	setFilePermission
	exportVariable
	exportCronjob
}
main

